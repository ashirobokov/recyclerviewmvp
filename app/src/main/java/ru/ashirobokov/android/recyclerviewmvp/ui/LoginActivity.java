package ru.ashirobokov.android.recyclerviewmvp.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import ru.ashirobokov.android.recyclerviewmvp.R;
import ru.ashirobokov.android.recyclerviewmvp.mvp.LoginInteractor;
import ru.ashirobokov.android.recyclerviewmvp.mvp.LoginPresenter;
import ru.ashirobokov.android.recyclerviewmvp.mvp.LoginPresenterImpl;
import ru.ashirobokov.android.recyclerviewmvp.mvp.LoginView;
import ru.ashirobokov.android.recyclerviewmvp.utils.ConstantManager;

/**
 * Created by Shirobokov on 18.01.2017.
 */

public class LoginActivity extends AppCompatActivity implements LoginView {

    static final String TAG = ConstantManager.TAG_PREFIX + "MainActivity";

    private ProgressBar mProgress;
    private EditText userEmail;
    private EditText userPass;
    private Button enterButton;
    private LoginPresenter presenter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_login);

        mProgress = (ProgressBar) findViewById(R.id.login_progress);
        userEmail = (EditText) findViewById(R.id.user_email);
        userPass = (EditText) findViewById(R.id.user_password);
        enterButton = (Button) findViewById(R.id.button);

        enterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.validateUser(userEmail.getText().toString(), userPass.getText().toString());
            }
        });


        presenter = new LoginPresenterImpl(this);

    }

    @Override
    protected void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public  void showProgress() {
        mProgress.setVisibility(View.VISIBLE);

    }

    @Override
    public  void hideProgress() {
        mProgress.setVisibility(View.GONE);

    }

    @Override
    public void setEmailError() {
         userEmail.setError("Ошибка в емайл адресе");

    }

    @Override
    public void setPasswordError() {
        userPass.setError("Пароль меньше 5 символов");

    }

    @Override
    public void navigateToHome() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }


}
