package ru.ashirobokov.android.recyclerviewmvp.mvp;

/**
 * Created by Shirobokov on 18.01.2017.
 */

public interface LoginInteractor {

    interface OnLoginFinishedListener {
        void onEmailError();

        void onPasswordError();

        void onSuccess();

    }

    void login(String email, String password, OnLoginFinishedListener listener);

}
