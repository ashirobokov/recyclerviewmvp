package ru.ashirobokov.android.recyclerviewmvp.mvp;

import android.os.Handler;
import android.text.TextUtils;

/**
 * Created by Shirobokov on 18.01.2017.
 */

public class LoginInteractorImpl implements LoginInteractor {

    @Override
    public void login(final String email, final String password, final OnLoginFinishedListener listener) {

        new Handler().postDelayed(new Runnable() {
            @Override public void run() {
                boolean error = false;
                if (!email.contains("@")){
                    listener.onEmailError();
                    error = true;
                    return;
                }
                if (password.length() < 5){
                    listener.onPasswordError();
                    error = true;
                    return;
                }
                if (!error){
                    listener.onSuccess();
                }
            }
        }, 2000);

    }

}
