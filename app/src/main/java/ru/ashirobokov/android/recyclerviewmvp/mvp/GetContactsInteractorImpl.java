package ru.ashirobokov.android.recyclerviewmvp.mvp;

import android.os.Handler;
import java.util.List;

import ru.ashirobokov.android.recyclerviewmvp.model.Contact;

/**
 * Created by Ashirobokov on 18.01.2017.
 */
public class GetContactsInteractorImpl implements GetContactsInteractor {

    private List<Contact> contacts;

    public GetContactsInteractorImpl() {

        contacts = Contact.createContactsList(15);

    }

    @Override
    public void createContacts(final OnGetContactsListener listener) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                listener.onFinished(getContacts());
            }
        }, 2000);

    }

    private List<Contact> getContacts() {

     return contacts;
    }

}
