package ru.ashirobokov.android.recyclerviewmvp.mvp;

/**
 * Created by Shirobokov on 18.01.2017.
 */

public interface LoginPresenter {

    void validateUser(String username, String password);

    void onDestroy();

}
