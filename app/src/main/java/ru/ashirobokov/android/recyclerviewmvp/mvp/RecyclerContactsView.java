package ru.ashirobokov.android.recyclerviewmvp.mvp;

import java.util.List;

import ru.ashirobokov.android.recyclerviewmvp.model.Contact;

/**
 * Created by Shirobokov on 15.01.2017.
 */

public interface RecyclerContactsView {

        void showProgress();

        void hideProgress();

        void setContacts(List<Contact> contacts);

        void showMessage(String message);

}


