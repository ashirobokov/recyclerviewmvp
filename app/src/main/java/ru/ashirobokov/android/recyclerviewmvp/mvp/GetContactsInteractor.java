package ru.ashirobokov.android.recyclerviewmvp.mvp;

import java.util.List;

import ru.ashirobokov.android.recyclerviewmvp.model.Contact;

/**
 * Created by Ashirobokov on 18.01.2017.
 */
public interface GetContactsInteractor {

        interface OnGetContactsListener {
                void onFinished(List<Contact> items);
        }

        void createContacts(OnGetContactsListener listener);

}
