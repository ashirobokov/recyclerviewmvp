package ru.ashirobokov.android.recyclerviewmvp.mvp;

import java.util.List;

import ru.ashirobokov.android.recyclerviewmvp.model.Contact;

/**
 * Created by Shirobokov on 15.01.2017.
 */

public class RecyclerContactsPresenterImpl implements RecyclerContactsPresenter, GetContactsInteractor.OnGetContactsListener {

    private RecyclerContactsView contactsView;
    private GetContactsInteractor getContactsInteractor;

    public RecyclerContactsPresenterImpl(RecyclerContactsView contactsView, GetContactsInteractor getContactsInteractor) {
            this.contactsView = contactsView;
            this.getContactsInteractor = getContactsInteractor;
    }

    @Override
    public void onFinished(List<Contact> contacts) {
        if (contactsView != null) {
            contactsView.setContacts(contacts);
            contactsView.hideProgress();
        }

    }

    @Override
    public  void onResume() {

        if (contactsView != null) {
            contactsView.showProgress();
            getContactsInteractor.createContacts(this);
        }

    }

    @Override
    public void onDestroy() {
        contactsView = null;
    }

    public RecyclerContactsView getContactsView() {
        return contactsView;
    }

}
