package ru.ashirobokov.android.recyclerviewmvp.mvp;

/**
 * Created by Shirobokov on 15.01.2017.
 */

public interface RecyclerContactsPresenter {

        void onResume();

        void onDestroy();

}
