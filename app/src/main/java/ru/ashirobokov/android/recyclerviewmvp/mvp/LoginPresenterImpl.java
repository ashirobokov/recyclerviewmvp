package ru.ashirobokov.android.recyclerviewmvp.mvp;

/**
 * Created by Shirobokov on 18.01.2017.
 */

public class LoginPresenterImpl implements LoginPresenter, LoginInteractor.OnLoginFinishedListener {

    private LoginView loginView;
    private LoginInteractor loginInteractor;

    public LoginPresenterImpl(LoginView loginView) {
        this.loginView = loginView;
        this.loginInteractor = new LoginInteractorImpl();
    }

    @Override
    public void validateUser(String email, String password) {
        if (loginView != null) {
            loginView.showProgress();
            loginInteractor.login(email, password, this);
        }

    }

    @Override
    public void onDestroy() {
        loginView = null;
    }


    @Override
    public void onEmailError() {
        if (loginView != null) {
            loginView.setEmailError();
            loginView.hideProgress();
        }

    }

    @Override
    public void onPasswordError() {
        if (loginView != null) {
            loginView.setPasswordError();
            loginView.hideProgress();
        }

    }

    @Override
    public void onSuccess() {
        if (loginView != null) {
            loginView.navigateToHome();
        }

    }


    public LoginInteractor getLoginInteractor() {
            return loginInteractor;
    }

    public LoginView getLoginView() {
        return loginView;
    }

}
