package ru.ashirobokov.android.recyclerviewmvp.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import java.util.List;

import ru.ashirobokov.android.recyclerviewmvp.R;
import ru.ashirobokov.android.recyclerviewmvp.data.ContactAdapter;
import ru.ashirobokov.android.recyclerviewmvp.model.Contact;
import ru.ashirobokov.android.recyclerviewmvp.mvp.GetContactsInteractorImpl;
import ru.ashirobokov.android.recyclerviewmvp.mvp.RecyclerContactsPresenter;
import ru.ashirobokov.android.recyclerviewmvp.mvp.RecyclerContactsPresenterImpl;
import ru.ashirobokov.android.recyclerviewmvp.mvp.RecyclerContactsView;
import ru.ashirobokov.android.recyclerviewmvp.utils.ConstantManager;

/**
 * Created by Shirobokov on 14.01.2017.
 */

public class MainActivity extends AppCompatActivity implements RecyclerContactsView {

   static final String TAG = ConstantManager.TAG_PREFIX + "MainActivity";

    private ProgressBar mProgress;
    private Toolbar mToolBar;
    private RecyclerView recyclerView;
    private RecyclerContactsPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.coordinator_layout);
        mProgress = (ProgressBar) findViewById(R.id.progress);
        mToolBar = (Toolbar) findViewById(R.id.app_toolbar);
        setSupportActionBar(mToolBar);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);

        presenter = new RecyclerContactsPresenterImpl(this, new GetContactsInteractorImpl());

    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onResume();
    }

    @Override
    protected void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public void showProgress() {
        mProgress.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.INVISIBLE);
    }


    @Override
    public void hideProgress() {
        mProgress.setVisibility(View.INVISIBLE);
        recyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void setContacts(List<Contact> contacts) {

        ContactAdapter adapter = new ContactAdapter(this, contacts);
        recyclerView.setAdapter(adapter);

    }

    @Override
    public void showMessage(String message) {
        Log.d(TAG, message);
    }


}
