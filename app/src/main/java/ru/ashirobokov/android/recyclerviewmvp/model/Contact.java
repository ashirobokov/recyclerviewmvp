package ru.ashirobokov.android.recyclerviewmvp.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Shirobokov on 14.01.2017.
 */

public class Contact {

    private String mName;
    private boolean mOnline;
    private static int lastContactId = 0;


    public Contact(String name, boolean online) {
        mName = name;
        mOnline = online;
    }

    public String getName() {
        return mName;
    }

    public boolean isOnline() {
        return mOnline;
    }

    public static List<Contact> createContactsList(int numContacts) {
        List<Contact> contacts = new ArrayList<Contact>();

        for (int i = 1; i <= numContacts; i++) {
            contacts.add(new Contact("Person   " + ++lastContactId, 0 == i % 2));
        }

        return contacts;
    }

}
