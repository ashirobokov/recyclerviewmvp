package ru.ashirobokov.android.recyclerviewmvp.mvp;

/**
 * Created by Shirobokov on 18.01.2017.
 */

public interface LoginView {

    void showProgress();

    void hideProgress();

    void setEmailError();

    void setPasswordError();

    void navigateToHome();

}
