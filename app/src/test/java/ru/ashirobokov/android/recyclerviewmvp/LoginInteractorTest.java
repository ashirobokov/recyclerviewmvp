package ru.ashirobokov.android.recyclerviewmvp;

import android.os.Handler;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import ru.ashirobokov.android.recyclerviewmvp.mvp.LoginInteractor;
import ru.ashirobokov.android.recyclerviewmvp.mvp.LoginInteractorImpl;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;



/**
 * Created by Shirobokov on 22.01.2017.
 */

@RunWith(MockitoJUnitRunner.class)
public class LoginInteractorTest {

//    private final static ScheduledExecutorService mainThread = Executors.newSingleThreadScheduledExecutor();

    @Mock
    LoginInteractor.OnLoginFinishedListener listener;

    private LoginInteractorImpl interactor;

    @Before
    public void setUp() throws Exception {
            interactor = new LoginInteractorImpl();
    }

    @Test
    public void checkOnSuccessInteraction() {


//  протестировать с Handler-ом пока не получается
//
//        Handler handler = mock(Handler.class);
//        Answer<Boolean> handlerPostAnswer = new Answer<Boolean>() {
//                        @Override
//                        public Boolean answer(InvocationOnMock invocation) throws Throwable {
//                            Runnable runnable = invocation.getArgument(0);
//                                 Long delay = 0L;
//                                 if (invocation.getArguments().length > 1) {
//                                         delay = invocation.getArgument(1);
//                                     }
//                                 if (runnable != null) {
//                                     mainThread.schedule(runnable, delay, TimeUnit.MILLISECONDS);
//                                 }
//
//                            return true;
//                             }
//        };

//        doAnswer(handlerPostAnswer).when(handler).postDelayed(any(Runnable.class), anyLong());

//        doAnswer(new Answer<Void>() {
//
//            @Override
//            public Void answer(InvocationOnMock invocation) throws Throwable {
//                return null;
//            }
//        }).when(listener).onSuccess();

        interactor.login("abc@abc.com", "qwerty", listener);

        verify(listener, times(1)).onSuccess();

        verify(listener, never()).onEmailError();
        verify(listener, never()).onPasswordError();

    }

    @Test
    public void checkOnEmailErrorInteraction() {

        interactor.login("abc1abc.com", "qwerty", listener);

        verify(listener, times(1)).onEmailError();

        verify(listener, never()).onSuccess();
        verify(listener, never()).onPasswordError();

    }

    @Test
    public void checkOnPasswordErrorInteraction() {

        interactor.login("abc@abc.com", "qwe", listener);

        verify(listener, times(1)).onPasswordError();

        verify(listener, never()).onSuccess();
        verify(listener, never()).onEmailError();

    }

}
