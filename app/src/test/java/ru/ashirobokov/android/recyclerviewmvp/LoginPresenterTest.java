package ru.ashirobokov.android.recyclerviewmvp;

import android.os.Handler;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import ru.ashirobokov.android.recyclerviewmvp.mvp.LoginInteractor;
import ru.ashirobokov.android.recyclerviewmvp.mvp.LoginInteractorImpl;
import ru.ashirobokov.android.recyclerviewmvp.mvp.LoginPresenter;
import ru.ashirobokov.android.recyclerviewmvp.mvp.LoginPresenterImpl;
import ru.ashirobokov.android.recyclerviewmvp.mvp.LoginView;

import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Created by Shirobokov on 22.01.2017.
 */

@RunWith(MockitoJUnitRunner.class)
public class LoginPresenterTest {

    @Mock
    LoginView view;
    @Mock
    LoginInteractor interactor;

    private LoginPresenterImpl presenter;

    @Before
    public void setUp() throws Exception {
            presenter = new LoginPresenterImpl(view);
    }


    @Test
    public void checkIfOnDestroy() {
        presenter.onDestroy();
        assertNull (presenter.getLoginView());

    }

}
