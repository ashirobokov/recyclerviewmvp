package ru.ashirobokov.android.recyclerviewmvp;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import ru.ashirobokov.android.recyclerviewmvp.model.Contact;
import ru.ashirobokov.android.recyclerviewmvp.mvp.*;

import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentCaptor.forClass;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ashirobokov on 20.01.2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class RecyclerContactsPresenterTest {

    @Mock
    RecyclerContactsView view;
    @Mock
    GetContactsInteractor interactor;

    private RecyclerContactsPresenterImpl presenter;

    @Before
    public void setUp() throws Exception {
        presenter = new RecyclerContactsPresenterImpl(view, interactor);
    }

    @Test
    public void checkIfViewIsReleasedOnDestroy() {
        presenter.onDestroy();
        assertNull (presenter.getContactsView());
    }


    @Test
    public void checkIfShowsProgressOnResume() {
        presenter.onResume();
        verify(view, times(1)).showProgress();
    }


///*
//    @Test
//    public void checkIfShowsMessageOnItemClick() {
//        presenter.onItemClicked(1);
//        verify(view, times(1)).showMessage(anyString());
//    }
//
//
//    @Test
//    public void checkIfRightMessageIsDisplayed() {
//        ArgumentCaptor<String> captor = forClass(String.class);
//        presenter.onItemClicked(1);
//        verify(view, times(1)).showMessage(captor.capture());
//        assertThat(captor.getValue(), is("Position 2 clicked"));
//    }
//*/

    @Test
    public void checkIfItemsArePassedToView() {
        List<Contact> items = new ArrayList<Contact>();
        items.add(new Contact("Person1", false));
        items.add(new Contact("Person2", true));

        presenter.onFinished(items);

        verify(view, times(1)).setContacts(items);
        verify(view, times(1)).hideProgress();
    }

}
